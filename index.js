let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon:["Pikachu","Charizard","Squirtle","Bullbasaur"],
	friends : {
		hoenn:["May","Max"],
		kanto:["Brock","Misty"]
		},
	talk: function(){
		console.log(trainer.pokemon[0]+ " I Choose You!");
	}
}

console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of bracket notation:");
console.log(trainer.pokemon);

console.log("Result of talk method:");

trainer.talk();

function Pokemon(name,level){
	this.name = name;
	this.level = level;
	this.health =  3*level ;
	this.attack=  1.5*level;
	this.tackle= function(pokemon){
		console.log(this.name + " tackled " + pokemon.name );
		if(this.attack > pokemon.health){
			pokemon.faint();
		}
	}
	this.faint= function(pokemon){
	console.log(this.name + " has fainted");
	}
}
let pokemon1 = new Pokemon("Pikachu",12);
console.log(pokemon1);
let pokemon2 = new Pokemon("Geodud",8);
console.log(pokemon2);
let pokemon3 = new Pokemon("MewTwo",100);
console.log(pokemon3);

pokemon3.tackle(pokemon2);


